from django.http import HttpResponse
from django.shortcuts import render
from post.serializers import QuestionSerializer

from django.views.generic import ListView, DetailView
from django.views.generic import TemplateView
from .models import  Question, Category
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status


class IndexView(ListView):
    model = Question
    template_name = "index.html"
    paginate_by = 5

    def get_queryset(self):
        # import pdb; pdb.set_trace()        
        return self.model.objects.all()
    
    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        print "context", context
        categories = Category.objects.all()
        context['categories']=categories
        # import pdb; pdb.set_trace()    
        return context

class CategoryView(ListView):
    model = Question
    template_name = "category_index.html"
    
    def get_queryset(self):
        # import pdb; pdb.set_trace()
        slug = self.kwargs.get('slug')
        return Question.objects.filter(category__slug=slug)
    
    def get_context_data(self, **kwargs):
        context = super(CategoryView, self).get_context_data(**kwargs)
        categories = Category.objects.all()
        context['categories']=categories
        # import pdb; pdb.set_trace()        
        return context


class PostDetailView(DetailView):
    model = Question
    template_name = "post_detail.html"
    # def get_context_data(self, **kwargs):
    #     context = super(PostDetailView, self).get_context_data(**kwargs)
    #     import pdb; pdb.set_trace()
    #     return context

class QuestionListView(APIView):
    def get(self, request, format=None):
        max_id = request.query_params.get('max_id')
        questions = Question.objects.filter(pk__lt=max_id)[:5]
        serializer = QuestionSerializer(questions, many=True)
        return Response(serializer.data)
