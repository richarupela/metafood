from django.contrib import admin

from .models import Question
from .models import Answer
from .models import Category
from .models import Follow

class QuestionAdmin(admin.ModelAdmin):
    list_filter = ('category',)
    pass

admin.site.register(Question, QuestionAdmin)
admin.site.register(Answer)
admin.site.register(Category)
admin.site.register(Follow)


