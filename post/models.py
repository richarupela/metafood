from __future__ import unicode_literals

from django.db import models
from django.utils.text import slugify
from django.contrib.auth.models import User

from taggit.managers import TaggableManager
from django.core.urlresolvers import reverse
from django.db.models.signals import pre_save


class Category(models.Model):
    title = models.CharField(max_length=400)
    slug = models.SlugField()

    def __unicode__(self):
        return self.title

class Question(models.Model):
    title = models.CharField(max_length=400)
    slug = models.SlugField(editable=False)
    description = models.TextField()
    tags = TaggableManager()
    created = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    author = models.ForeignKey(User)
    category = models.ManyToManyField(Category)

    def get_absolute_url(self):
        return reverse("question:detail", kwargs={"slug": self.slug})

    def __unicode__(self):
        return self.title
    
    class Meta:
        ordering = ("-created",)

class Answer(models.Model):
    question = models.ForeignKey(Question)    
    description = models.TextField()
    created = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    author = models.ForeignKey(User)

    def __unicode__(self):
        return self.description

class Follow(models.Model):
    user = models.OneToOneField(User)
    category = models.ManyToManyField(Category)

    def __unicode__(self):
        return self.user.username + '  -  ' + "-".join(self.category.all().values_list('title', flat=True))

def create_slug(instance, new_slug=None):
    slug = slugify(instance.title)
    print "Created slug ", slug
    if new_slug is not None:
        slug = new_slug
        print "updated slug ", slug
    qs = Question.objects.filter(slug=slug).order_by("-id")
    print "qs ", qs
    exists = qs.exists()
    if exists:
        new_slug = "%s-%s" % (slug, qs.first().id)
        print "id", qs.first().id
        print "NEW SLUG", new_slug
        return create_slug(instance, new_slug=new_slug)
    return slug


def pre_save_post_reciever(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = create_slug(instance)

pre_save.connect(pre_save_post_reciever, sender=Question)
