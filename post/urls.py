from django.conf.urls import url
from .views import (
	IndexView,
	PostDetailView,
    QuestionListView,
    CategoryView
)

app_name = 'post'

urlpatterns = [
    url(r'^$', IndexView.as_view()),
    url(r'^question/(?P<slug>[\w-]+)$', PostDetailView.as_view(), name="detail"),
    url(r'^category/(?P<slug>[\w-]+)$', CategoryView.as_view(), name="category_list"),    
    url(r'^api/questions/$', QuestionListView.as_view(), name="list"),
]
